// ==UserScript==
// @name          TagPro Isometric
// @description   Replace textures in tagpro
// @version       1
// @grant         none
// @include       http://tagpro-*.koalabeast.com:*
// @include       http://tangent.jukejuice.com:*
// @include       http://*.newcompte.fr:*
// @license       2014
// @author        whatever
// ==/UserScript==

tagpro.loadAssets({
  "tiles": "http://i.imgur.com/muSWwYQ.png",
  "speedpad": "http://i.imgur.com/d54tO8W.png",
  "speedpadRed": "http://i.imgur.com/3hZvUC8.png",
  "speedpadBlue": "http://i.imgur.com/Wskg3sK.png",
  "portal": "http://i.imgur.com/592SA0f.png",
  "splats": "http://i.imgur.com/J5arhbQ.png"
});